# 1. Preparation and test on home machines
- [ ] Check differences in config file
- [ ] Update theme
- [ ] Test updating on home server with host_vars

# 2. Staging
- [ ] PR for role on Staging created
- [ ] PR for role on Staging merged
- [ ] PR for host_vars on Staging created
- [ ] PR for host_vars on Staging merged
- [ ] Antilopa's approval for Staging
- [ ] Fede's approval for Staging
- [ ] Avg_joe's approval for Staging

# 3. Production
- [ ] PR for role on Main created
- [ ] PR for role on Main merged
- [ ] PR for host_vars on Main created
- [ ] PR for host_vars on Main merged
